package java.awt;

import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class Graphics2D extends Graphics {
    public void dispose() {

    }

    public void setColor(Color bg) {

    }

    public void fillRect(int x, int y, int w, int h) {

    }

    public void drawImage(Image image, AffineTransform identity, Object o) {

    }

    public void drawImage(BufferedImage image, int i, int i1, Object o) {

    }

    public AffineTransform getTransform() {
        return null;
    }
}
