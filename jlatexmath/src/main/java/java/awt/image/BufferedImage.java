package java.awt.image;

import android.graphics.Bitmap;

import java.awt.Graphics2D;
import java.awt.Image;

public class BufferedImage implements Image {
    public static final Bitmap.Config TYPE_INT_ARGB = Bitmap.Config.ARGB_8888;
    public static final Bitmap.Config TYPE_INT_RGB = Bitmap.Config.RGB_565;

    public BufferedImage(int width, int height, Bitmap.Config config) {
    }

    public Graphics2D createGraphics() {
        return null;
    }
}
